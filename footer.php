<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Landing_Page_Yeison_Reyes
 */

?>
    </div><!-- Esto cierra el row -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="container">
            <div class="row justify-content-between">
            <div class="col-md-4 redes">
			<a class="icono" href="https://www.facebook.com/gamultimarca/" target="_blank"><img class="" src="/wp-content/uploads/2019/11/001-facebook.png" alt="" href="https://www.facebook.com/gamultimarca/" target="_blank"></img></a>
			<a class="icono" href="https://www.instagram.com/grupomultimarca/" target="_blank"><img class="" src="/wp-content/uploads/2019/11/002-instagram.png" alt="" href="https://www.instagram.com/grupomultimarca/" target="_blank"></img></a>
			<a class="icono" href="https://twitter.com/GrupoMultimarca" target="_blank"><img class="" src="/wp-content/uploads/2019/11/003-twitter-1.png" alt="" href="https://twitter.com/GrupoMultimarca" target="_blank"></img></a>
                    </div>
            <div class="col-md-4 .ml-auto redes">
			<a class="navbar-brand" href="#"><img class="logo_footer" src="/wp-content/uploads/2019/11/Emblema-Multimarca-blanco.png" class="img-fluid"></img></a>
                    </div>
            </div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 mt-2">
				<p class="subtitulo-footer">2019 <a href="https://www.multimarca.com.ve/" target="_blank">Multimarca® </a>| Desarrollado por <a class="footer" href="http://playfulagency.com/" target="_blank">Playful Agency. </a></p>
				</div>
			</div>

		</div>
    	
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
