<?php get_header();?>

<!-- Slider-->
<header class="masthead">
            <div class="container textos-header titulos-m">
                    <div class="row align-items-start">
                        <div class="col-12">
                        <h1 class="titulos-m .col-sm-12">LA MEJOR OPCIÓN DE COMPRA</h1>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                      <div class="col-3 fondo-azul-1 mt-5">
                        <h3 class="sticker-1">RESÉRVALO CON EL</h3><BR><h3 class="sticker-2">10%</h3>
                        </div>
                    </div>
                </div>
            </div>
  
    </div>
    </header>
<!-- Cuadros Azules -->
<div class="container">
    <section class="cuadros bg-light">
        <div class="row align-items-start">
                <div class="col-md-8 col-xs-12 separador-row d-flex flex-wrap">
                      <div class="col-md-6 col-xs-12 mb-4" >
                        <div class="banderin-1">
                                <h4 class="banderin">IMPORTADOS 0 KM</h4>
                                <p class="banderin">Nissan X-trail 2018 <br>Nissan Qashqai Rouge 2019<br> Toyota Corolla 2020</p>  
                        </div>
                        </div>
                        <div class="col-md-6 col-xs-12  mb-4">
                                <div class="banderin-1">
                                        <h4 class="banderin">A TU MEDIDA</h4>
                                        <p class="banderin">Tú eliges el color Las mejores condiciones del mercado.</p>
                                </div>       
                        </div>
                        <div class="col-md-6 col-xs-12 mb-4" >
                                <div class="banderin-1">
                                        <h4 class="banderin">MODALIDAD DE PAGO</h4>
                                        <p class="banderin">Puedes reservar y congelar el precio de compra.</p>
                                </div>
                        </div>
                        <div class="col-md-6 col-xs-12 mb-4" >
                                <div class="banderin-1">
                                        <h4 class="banderin">RÁPIDA ENTREGA</h4>
                                        <p class="banderin">Nos encargamos de los trámites para tu tranquilidad hasta el momento de la entrega.</p>
                                </div>
                        </div>
                        <div class="row justify-content-between mt-5">
                                <div class="col-md-12  caracteres">
                                        <h2 class="subtitulo"> Nissan X- trail 4x4 / 2018</h2>
                                        <hr>
                                        <p class="subtitulo"> Nissan All-Mode | 0 km. </p>
                                        <ul class="lista-completa">
                                                <li>• Nissan All-Mode 4x4-intuitivo</li>
                                                <li>• AVM Monitor + Detección de Objetos en Movimiento</li>
                                                <li>• Techo panorámico</li>
                                                <li>• Llantas de aleación de 18"</li>
                                                <li>• Salida del Carril</li>
                                                <li>• Advertencia de punto ciego</li>
                                                <li>• Combimetro de pantalla TFT a todo color HD de 5 "CD de Audio, MP3, USB, AUX</li>
                                                <li>• Asientos de cuero </li>
                                                <li>• Asientos delanteros con calefacción</li>
                                                <li>• Volante de cuero</li>
                                                <li>• Pomo del cambio</li>
                                                <li>• Bluetooth (Sistema Push-To-Talk) + Bluetooth (Streaming Music)</li>
                                                <li>• Entrada inteligente sin llave con control de las puertas y el portón trasero</li>
                                                <li>• Operación sin llave-botón de inicio</li>
                                                <li>• Parada, eléctrico y espejos laterales plegables con señal de giro</li>
                                                <li>• Faros antiniebla delanteros con molduras cromadas</li>
                                                <li>• Aire acondicionado automático con control de clima de doble zona</li>
                                        </ul>
                                        <div id="carouselExampleFade" class="carousel slide margen-carrusel" data-ride="carousel">
                                                <div class="carousel-inner">
                                                        <div class="carousel-item active" >
                                                                <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Xtrail-1.png" class="d-block w-100" alt="Nissan-Rouge-Xtrail-1">
                                                        </div>
                                                        <div class="carousel-item">
                                                                <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Xtrail-2.png" class="d-block w-100" alt="Nissan-Rouge-Xtrail-1">
                                                        </div>
                                                        <div class="carousel-item">
                                                                <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Xtrail-3.png" class="d-block w-100" alt="Nissan-Rouge-Xtrail-1">
                                                        </div>
                                                        <div class="carousel-item">
                                                                <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Xtrail-4.png" class="d-block w-100" alt="Nissan-Rouge-Xtrail-1">
                                                        </div>
                                                </div>

                                        </div>
                                        <button type="button" class="btn btn-primary btn-lg btn-block"><a href="#sidebar-movil"><a id="link" href="#sidebar"> Reserva Nissan X- trail 4x4 2018</a>
                                                                                <a id="link-2" href="#sidebar-movil"> Reserva Nissan X- trail 4x4 2018</a></a>
                                        </button>
                                </div>
                        </div><!--aqui cierro seccion de row --> 
                </div>     <!--aqui cierro seccion de col-8--> 
                <div class="col-4 borde d-none d-sm-none d-md-block" id="sidebar">
                        <?php dynamic_sidebar(); ?>
                </div> 
        </section>
</div>

<!-- Nissan XTRAil -->
<section class="features-icons bg-light text-center">
    <!-- SEPARACION ENTRE AUTOS -->            
   <!-- Nissan Qashqai Rouge / 2019-->
    <section class="features-icons bg-light text-center">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-8">
                        <h2 class="subtitulo"> Nissan Qashqai Rouge / 2019</h2>
                        <hr>
                        <p class="subtitulo"> Nissan Qashqai Rouge | 0 km. </p>
                        <ul class="lista-completa">
                                <li>• Llave inteligente (2 llaveros) con alarma de pánico e inmovilizador</li>
                                <li>• Vista trasera Cámara</li>
                                <li>• Llantas de aleación de 18"</li>
                                <li>• Barras de techo plateadas</li>
                                <li>• Sensores de estacionamiento FR + RR</li>
                                <li>• Faros halógenos</li>
                                <li>• Neumáticos: 215/55 R18</li>
                                <li>• Antena de aleta de tiburón</li>
                                <li>• Control de crucero con dirección</li>
                                <li>• SW para medidor, audio, teléfono y ASCD</li>
                                <li>• Motor 2.0, Gasolina, 4 cilindros</li>
                                <li>• Asientos de tela</li>
                        </ul>
                </div>
                <div class="col-6"></div>             
                <div class="col-md-8">
                        <div id="carouselExampleFade" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                <div class="carousel-item active" >
                                        <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Qashqai-1.png" class="d-block w-100" alt="Nissan Qashqai Rouge / 2019">
                                </div>
                                <div class="carousel-item">
                                        <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Qashqai-2.png" class="d-block w-100" alt="Nissan Qashqai Rouge / 2019">
                                </div>
                                <div class="carousel-item">
                                        <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Qashqai-3.png" class="d-block w-100" alt="Nissan Qashqai Rouge / 2019">
                                </div>
                                <div class="carousel-item">
                                        <img src="/wp-content/uploads/2019/11/Nissan-Rouge-Qashqai-4.png" class="d-block w-100" alt="Nissan Qashqai Rouge / 2019">
                                </div>
                        </div>
                        
                </div>
                <button type="button" class="btn btn-primary btn-lg btn-block"><a href="#sidebar" id="link">Reserva Nissan Qashqai Rouge 2019</a><a id="link-2" href="#sidebar-movil"> Reserva Nissan Qashqai Rouge 2019</a></button>
        </div>
</div>
</div>
</div>
<!--  Toyota Corolla 2020 -->
<section class="features-icons bg-light text-center">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-8">
                        <h2 class="subtitulo"> Toyota Corolla 2020</h2>
                        <hr>
                        <p class="subtitulo"> Toyota Corolla | 0 km. </p>
                        <ul class="lista-completa">
                                <li>• Motor: 1.8L 4 cilindros</li>
                                <li>• SEFI, VVT-i Dual, ETCS-i</li>
                                <li>• Potencia: 140 CV a 6 100 rpm</li>
                                <li>• Par de apriete: 126 lb-ft @ 3 900 rpm</li>
                                <li>• Tracción delantera</li>
                                <li>• Ruedas Acero de 16"</li>
                                <li>• Asiento del conductor de 6 posiciones ajustable manualmente</li>
                                <li>• Asiento trasero dividido 60/60</li>
                                <li>• AM / FM / MP3 / WMA / Aux- sistema de audio in / USB con pantalla táctil de 7" y 6 bocinas</li>
                                <li>• Bluetooth con controles montados en el volante</li>
                        </ul>
                </div>
                <div class="col-6"></div>             
                <div class="col-md-8">
                        <div id="carouselExampleFade" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                        <div class="carousel-item active">
                                                <img src="/wp-content/uploads/2019/11/corolla-2020-1.png" class="d-block w-100" alt="Toyota Corolla 2020">
                                        </div>
                                        <div class="carousel-item">
                                                <img src="/wp-content/uploads/2019/11/corolla-2020-2-1.png" class="d-block w-100" alt="Toyota Corolla 2020">
                                        </div>
                                        <div class="carousel-item">
                                                <img src="/wp-content/uploads/2019/11/corolla-2020-3-1.png" class="d-block w-100" alt="Toyota Corolla 2020">
                                        </div>
                                </div>
                               
                        </div>
                        <button type="button" class="btn btn-primary btn-lg btn-block"><a href="#sidebar" id="link"> Reserva Toyota Corolla 2020</a><a id="link-2" href="#sidebar-movil">Reserva Toyota Corolla 2020</a></button>
                </div>
            </div>
        </div>
</div>
<div class="borde d-block d-sm-block d-md-none" id="sidebar-movil"><?php dynamic_sidebar(); ?></div> 
<!-- CONTENEDOR DE INFO -->
<div class="container titulos-2 detener">
                <div class="row">
                        <div class="col-md-auto">  </div>
                        <div class="col-md-12 mt-5">
                                <p class="logo-2"><img src="/wp-content/uploads/2019/11/Icono-azul-scaled-2560.png" class="logo-azul" alt="..." align="center"></p>
                                <h1 class="titulo-2">¿Por qué nosotros?</h1>
                                <p class="texto-normal"> Somos un grupo automotriz con más de 28 años en Venezuela, que durante nuestro recorrido hemos brindado soluciones e innovación a nuestros clientes, 
                                        quienes respaldan nuestro éxito mediante su satisfacción. Esto nos ha consolidado como líderes en venta de vehículos 0 kms, usados e importados. 
                                        Seleccionamos en esta oportunidad modelos exclusivos y a la vanguardia que permitirán la actualización de su vehículo particular 
                                        brindándole confort y experiencia  ante las nuevas tecnologías que existen en el mundo.</p>
                                <p class="texto-normal"><b>MODELOS IMPORTADOS</b><br> Disponibles Nissan X-trail 2018, Nissan Qashqai Rouge 2019 y Toyota Corolla 2020. </p>
                                <p class="texto-normal"> <b>A TU GUSTO</b><br> Tienes la opción de elegir el color entre los anteriormente expuestos. <br>
                                <p class="texto-normal"><b>MODALIDAD DE PAGO</b><br> La oportunidad que esperabas de tan solo iniciar el proceso de importación con un 10% del valor del vehículo escogido.<br> 
                                <p class="texto-normal"><b>RÁPIDA ENTREGA</b><br> Nos encargamos de los trámites para tu tranquilidad hasta el momento de la entrega.</p>
                        </div>
                                <div class="row">
                                        <div class="col-1 col-xs-12"></div>
                                        <div class="col-12 col-xs-12 text-center">
                                                <button role="button" class="btn btn-second btn-lg btn-block boton_reserva" href="#sidebar" id="btn_10">
                                                        <a href="#sidebar">Reserva ya con el <span class="third-text">10%</span>
                                                                <p class="second-text"> y congela el precio</p>
                                                </button>
                                                <button  class="btn btn-four btn-lg btn-block boton_reserva "><a href="#sidebar-movil" id="btn_movil"> Reserva ya con el 10% y congela el precio</a>
                                                </button>
                                        <div class="col-2 col-xs-12"></div>
                                </div>
                        </div>
                </div>
    </div>


<?php get_footer(); ?>