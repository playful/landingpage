<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Landing_Page_Yeison_Reyes
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Grupo Multimarca | La mejor opción para comprar carros importados. Reserva con 10% y congela el precio de compra del vehículo. Nos encargamos de todos los trámites" >
	<link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">


	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>

<nav class="navbar-1">
        <div class="container">
            <div class="row justify-content-between">
            <div class="col-md-4 redes">
                <a class="navbar-brand" href="/index.php"><img class="" src="/wp-content/uploads/2019/11/Logo-1.png" class="img-fluid"></img></a>
                    </div>
            <div class="col-md-4 .ml-auto redes">
                <a class="icono" href="https://www.facebook.com/gamultimarca/" target="_blank">
                <img class="" src="/wp-content/uploads/2019/11/1-facebook.png" alt="" ></img></a>
                <a class="icono" href="https://www.instagram.com/grupomultimarca/" target="_blank">
                    <img class="" src="/wp-content/uploads/2019/11/2-instagram.png" alt="" ></img></a>
            
                            <a class="icono" href="https://twitter.com/GrupoMultimarca" target="_blank">
                                    <img class="" src="/wp-content/uploads/2019/11/3-twitter.png" alt="" ></img></a>
                    </div>
            </div>
        </div>
    </nav>
		</div><!-- .site-branding -->

    <!-- Navigation -->
    
	</div><!-- Engloba la estructura del menu-->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117997499-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117997499-2');
</script>
	</header><!-- #masthead -->

	
